package com.maapza.maapza.data.remote.model;



/**
 * Created by MacHD on 1/15/18.
 */

public class Place {
    private String id_place;
    private String name;
    private String description;
    private String main_photo;
    private Double latitude;
    private Double longitude;
    private String level;
    private String phone;
    private String address;
    private String sart_day;
    private String end_day;
    private String open_time;
    private String closed_time;
    private String id_category;
    private Float average_rating;
    private Float price_min;
    private Float price_max;

    public Place(){}

    public Place(String id_place, String name, String description, String main_photo) {
        this.id_place = id_place;
        this.name = name;
        this.description = description;
        this.main_photo = main_photo;
    }

    public Place(String id_place, String name, String description, String main_photo, Double latitude, Double longitude, String level, String phone, String address, String sart_day, String end_day, String open_time, String closed_time, String id_category, Float average_rating, Float price_min, Float price_max) {
        this.id_place = id_place;
        this.name = name;
        this.description = description;
        this.main_photo = main_photo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.level = level;
        this.phone = phone;
        this.address = address;
        this.sart_day = sart_day;
        this.end_day = end_day;
        this.open_time = open_time;
        this.closed_time = closed_time;
        this.id_category = id_category;
        this.average_rating = average_rating;
        this.price_min=price_min;
        this.price_max=price_max;
    }

    public String getId_place() {
        return id_place;
    }

    public void setId_place(String id_place) {
        this.id_place = id_place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMain_photo() {
        return main_photo;
    }

    public void setMain_photo(String main_photo) {
        this.main_photo = main_photo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSart_day() {
        return sart_day;
    }

    public void setSart_day(String sart_day) {
        this.sart_day = sart_day;
    }

    public String getEnd_day() {
        return end_day;
    }

    public void setEnd_day(String end_day) {
        this.end_day = end_day;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public String getClosed_time() {
        return closed_time;
    }

    public void setClosed_time(String closed_time) {
        this.closed_time = closed_time;
    }

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public Float getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(Float average_rating) {
        this.average_rating = average_rating;
    }
    public Float getPrice_min() {
        return price_min;
    }

    public void setPrice_min(Float price_min) {
        this.price_min = price_min;
    }

    public Float getPrice_max() {
        return price_max;
    }

    public void setPrice_max(Float price_max) {
        this.price_max = price_max;
    }
}
