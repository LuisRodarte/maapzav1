package com.maapza.maapza.data.remote.model;

public class Times {
    private String time_name, time_open, time_closed;
    private int id_time;

    public String getTime_name() {
        return time_name;
    }

    public void setTime_name(String time_name) {
        this.time_name = time_name;
    }

    public String getTime_open() {
        return time_open;
    }

    public void setTime_open(String time_open) {
        this.time_open = time_open;
    }

    public String getTime_closed() {
        return time_closed;
    }

    public void setTime_closed(String time_closed) {
        this.time_closed = time_closed;
    }

    public int getId_time() {
        return id_time;
    }

    public void setId_time(int id_time) {
        this.id_time = id_time;
    }
}
