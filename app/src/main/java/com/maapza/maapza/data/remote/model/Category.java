package com.maapza.maapza.data.remote.model;

import java.io.Serializable;

/**
 * Created by MacHD on 1/23/18.
 */

public class Category implements Serializable {

    private String id_category;
    private String name;
    private String icon;

    public Category() {
    }

    public Category(String id_category, String name, String icon) {
        this.id_category = id_category;
        this.name = name;
        this.icon = icon;
    }

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
