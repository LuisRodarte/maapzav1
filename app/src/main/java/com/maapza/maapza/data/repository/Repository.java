package com.maapza.maapza.data.repository;

import com.google.firebase.auth.FirebaseUser;

public interface Repository {
    FirebaseUser getCurrentUser();
}
