package com.maapza.maapza.data.remote.model;

/**
 * Created by MacHD on 6/25/18.
 */

public class User {
    private String email;
    private String id_user;
    private String name_user;
    private String photo_url;

    public User() {
    }

    public User(String email, String id_user, String name_user, String photo_url) {
        this.email = email;
        this.id_user = id_user;
        this.name_user = name_user;
        this.photo_url = photo_url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }
}
