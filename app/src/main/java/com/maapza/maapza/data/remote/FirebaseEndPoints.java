package com.maapza.maapza.data.remote;

public class FirebaseEndPoints {

    public static final String CATEGORY_ENDPOINT = "category";
    public static final String PLACE_ENDPOINT = "place";
    public static final String PLACE_IMAGES_ENDPOINT = "place_images";
    public static final String PLACE_REVIEWS_ENDPOINT = "reviews";
    public static final String PLACE_TIMES_ENDPOINT = "times";
    public static final String PLACE_RATES_ENDPOINT = "rates";

}
