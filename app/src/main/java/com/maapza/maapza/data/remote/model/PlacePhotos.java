package com.maapza.maapza.data.remote.model;

/**
 * Created by MacHD on 5/24/18.
 */

public class PlacePhotos {
    private int id_place;
    private String url;

    public PlacePhotos() {
    }

    public PlacePhotos(int id_place, String url) {
        this.id_place = id_place;
        this.url = url;
    }

    public int getId_place() {
        return id_place;
    }

    public void setId_place(int id_place) {
        this.id_place = id_place;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
