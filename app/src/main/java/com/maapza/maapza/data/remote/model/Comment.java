package com.maapza.maapza.data.remote.model;

/**
 * Created by LUIS.RODARTE on 22/05/2018.
 */

public class Comment {

    private String id_comment, id_user, name_user, url_photo, comment, comment_date;
    private Float rating;
    private int id_place;

public Comment(){}

    public Comment(String id_comment, String id_user, String name_user, String url_photo, Float rating, String comment, String comment_date, int id_place) {
        this.id_comment = id_comment;
        this.id_user = id_user;
        this.url_photo = url_photo;
        this.rating = rating;
        this.comment = comment;
        this.name_user = name_user;
        this.comment_date = comment_date;
        this.id_place=id_place;
    }

    public String getId_comment() {
        return id_comment;
    }

    public void setId_comment(String id_comment) {
        this.id_comment = id_comment;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName_user() {
        return name_user;
    }

    public void setName_user(String name_user) {
        this.name_user = name_user;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_date() {
        return comment_date;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }

    public int getId_place() {
        return id_place;
    }

    public void setId_place(int id_place) {
        this.id_place = id_place;
    }
}

