package com.maapza.maapza.ui.listplaces;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maapza.maapza.data.remote.model.Place;

import java.util.ArrayList;
import java.util.List;

import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_ENDPOINT;

public class ListPlacesInteractorImpl implements ListPlacesContract.Interactor {

    private DatabaseReference place;

    public ListPlacesInteractorImpl() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        place = database.getReference(PLACE_ENDPOINT);
    }

    @Override
    public void doGetPlaces(final OnFinishedListener onFinishedListener) {

        Query query = place.orderByChild("level").equalTo("1");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Place> placesList = new ArrayList<>();
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    Place place = categorySnapshot.getValue(Place.class);
                    placesList.add(place);
                }
                onFinishedListener.onFinishedGetPlaces(placesList);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
