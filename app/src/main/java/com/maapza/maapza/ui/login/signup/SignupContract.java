package com.maapza.maapza.ui.login.signup;

public interface SignupContract {

    interface View {
        void showSuccessMessage();

        void showErrorUserRegisteredMessage();

        void showErrorMessage(String msg);

        void showLoading(Boolean show);

        void closeActivity();

        void navToMainActivity();

    }

    interface Presenter {

        void registerUser(String email, String password, String username);
    }

    interface Interactor {
        interface OnFinishedListener {
            void onFinished();

            void onFailure(String msg);

            void onUserAlreadyRegistered();
        }

        void registerUser(String email, String password, String username, SignupContract.Interactor.OnFinishedListener onFinishedListener);

    }

}
