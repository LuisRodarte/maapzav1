package com.maapza.maapza.ui.map;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.CategorySuggestion;
import com.maapza.maapza.data.remote.model.InfoWindowData;
import com.maapza.maapza.ui.categories.CategoryRepository;
import com.maapza.maapza.ui.categories.ListCategoriesFragment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.ui.placedetail.PlaceFragment;
import com.maapza.maapza.utils.GPSTracker;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MapFragment extends Fragment {
    private View myFragmentView;
    MapView mMapView;
    GoogleMap map;
    ProgressDialog progressDialog;
    private String id_category = "";
    private String id_place = "";
    private Double lat, lng;
    private String name_category = "";
    private DatabaseReference place;
    private DatabaseReference category;
    private List<Place> placesList;
    private List<CategorySuggestion> suggestions = new ArrayList<>();
    private String mLastQuery = "";
    public static final long FIND_SUGGESTION_SIMULATED_DELAY = 250;
    private FloatingSearchView mSearchView;
    private CategoryRepository categoryRepository;
    GPSTracker gps;
    private static final int REQUEST_LOCATION = 1;
    private BitmapDescriptor iconMarker;

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //important! set your user agent to prevent getting banned from the osm servers
    }

    @Override
    public void onStart() {
        super.onStart();
        getCategories();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.activity_maps, container, false);
        mSearchView = myFragmentView.findViewById(R.id.floating_search_view);
        mSearchView.setCloseSearchOnKeyboardDismiss(true);
        mSearchView.setDismissFocusOnItemSelection(true);
        mMapView = (MapView) myFragmentView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext());
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                map = mMap;
                setMapStyle();
                Bundle bundle = getArguments();
                if (bundle != null) {
                    if (bundle.getString("action").equals("only_category")) {
                        //display places by category
                        id_category = bundle.getString("id_category");
                        name_category = bundle.getString("name_category");
                        mSearchView.setSearchText(name_category);
                        getPlacesbyIdCategory(id_category);
                        goCurrentPosition();
                    } else {
                        //display place by id
                        id_place = bundle.getString("id_place");
                        lat = bundle.getDouble("lat");
                        lng = bundle.getDouble("lng");
                        LatLng coord = new LatLng(lat, lng);
                        getPlacesById(id_place);
                        goToCoords(coord);
                    }

                } else {
                    //addSingleMarker(startPoint, "La Cazuela Mexican Restaurant", "1", 4f);
                    goCurrentPosition();
                }
                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                    @Override
                    public boolean onMarkerClick(final Marker mark) {


                        mark.showInfoWindow();

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mark.showInfoWindow();

                            }
                        }, 200);

                        return true;
                    }
                });


                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        navigateToPlaceFragment(marker.getSnippet());
                    }
                });

            }
        });


        setupSearchBar();
        iconMarker = BitmapDescriptorFactory.fromResource(R.drawable.map_marker3);

        return myFragmentView;
    }


    // Check for permission to access Location
    private boolean checkPermission() {
        // Ask for permission if it wasn't granted yet
        return (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // Asks for permission
    private void askPermission() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_LOCATION
        );
    }

    public void setMapStyle() {

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = map.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.map_style));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

    }

    public void goCurrentPosition() {

        if (checkPermission()) {
            map.setMyLocationEnabled(true);
            // Add a marker in Sydney and move the camera
            LatLng currentLoc = getCurrentLocation();
            goToCoords(currentLoc);
            //map.addMarker(new MarkerOptions().position(currentLoc).title("My Location"));
            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLoc).zoom(14).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            askPermission();
        }

    }

    public void goToCoords(LatLng coords) {

        CameraPosition cameraPosition = new CameraPosition.Builder().target(coords).zoom(14).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPermission())
                        goCurrentPosition();
                } else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    gps.showSettingsAlert();
                }
            }
            return;
        }

    }


    public void addSingleMarker(LatLng coord, String namePlace, String id_place, Float rating, String imgUrl) {

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(coord)
                .title(namePlace)
                .snippet(id_place)
                .icon(iconMarker);
        InfoWindowData info = new InfoWindowData();
        info.setImage(imgUrl);
        info.setRating(rating);
        info.setRating(rating);
        CustomInfoWindowMap customInfoWindow = new CustomInfoWindowMap(getContext());
        map.setInfoWindowAdapter(customInfoWindow);
        Marker m = map.addMarker(markerOptions);
        m.setTag(info);
    }

    public void getPlacesbyIdCategory(String id_category) {
        showProgressDialog("Cargando...");
        map.clear();
        placesList = new ArrayList<>();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        place = database.getReference("place");
        Query query = place.orderByChild("id_category").equalTo(id_category);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    if (placesList != null) {
                        placesList.clear();
                    }
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        Place place = categorySnapshot.getValue(Place.class);
                        placesList.add(place);
                    }
                    for (Place item : placesList) {
                        LatLng coord = new LatLng(item.getLatitude(), item.getLongitude());
                        addSingleMarker(coord, item.getName(), item.getId_place(), item.getAverage_rating(), item.getMain_photo());
                    }

                } else {
                    hideProgressDialog();
                    Toast.makeText(getContext(), "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                }
                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
    }

    public void getPlacesById(String id_place) {
        showProgressDialog("Cargando...");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        place = database.getReference("place").child(id_place);
        map.clear();
        place.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Place place = dataSnapshot.getValue(Place.class);
                    LatLng coord = new LatLng(place.getLatitude(), place.getLongitude());
                    addSingleMarker(coord, place.getName(), place.getId_place(), place.getAverage_rating(), place.getMain_photo());
                } else {
                    hideProgressDialog();
                    Toast.makeText(getContext(), "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                }
                hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideProgressDialog();
            }
        });
    }

    public void getAllCategories() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        category = database.getReference("category");
        Query query = place.orderByChild("id_category").equalTo(id_category);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    if (placesList != null) {
                        placesList.clear();
                    }
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        Place place = categorySnapshot.getValue(Place.class);
                        placesList.add(place);
                    }
                    for (Place item : placesList) {
                        LatLng coord = new LatLng(item.getLatitude(), item.getLongitude());
                        addSingleMarker(coord, item.getName(), item.getId_place(), item.getAverage_rating(), item.getMain_photo());
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getCategories() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        category = database.getReference("category");
        category.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (suggestions != null) {
                    suggestions.clear();
                }
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    CategorySuggestion category = categorySnapshot.getValue(CategorySuggestion.class);
                    suggestions.add(category);

                }
                categoryRepository = new CategoryRepository(suggestions);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    public LatLng getCurrentLocation() {
        double lat = 0.0;
        double lng = -0.0;
        gps = new GPSTracker(getContext(), getActivity());
        // Check if GPS enabled
        if (gps.canGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
        } else {
            // Can't get location.
            // GPS or network is not enabled.
            // Ask user to enable GPS/network in settings.
            gps.showSettingsAlert();
        }
        LatLng currentLocation = new LatLng(lat, lng);
        return currentLocation;
    }


    private void setupSearchBar() {
        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                if (!oldQuery.equals("") && newQuery.equals("")) {
                    mSearchView.clearSuggestions();
                } else {

                    //this shows the top left circular progress
                    //you can call it where ever you want, but
                    //it makes sense to do it when loading something in
                    //the background.
                    mSearchView.showProgress();
                    //simulates a query call to a data source
                    //with a new query.
                    categoryRepository.findSuggestions(getContext(), newQuery, 10,
                            FIND_SUGGESTION_SIMULATED_DELAY, new CategoryRepository.OnFindSuggestionsListener() {

                                @Override
                                public void onResults(List<CategorySuggestion> results) {

                                    //this will swap the data and
                                    //render the collapse/expand animations as necessary
                                    mSearchView.swapSuggestions(results);

                                    //let the users know that the background
                                    //process has completed
                                    mSearchView.hideProgress();
                                }
                            });
                }

                Log.d("LOG", "onSearchTextChanged()");
            }
        });

        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(final SearchSuggestion searchSuggestion) {

                CategorySuggestion suggestion = (CategorySuggestion) searchSuggestion;
                getPlacesbyIdCategory(suggestion.getId_category());
              /*  DataHelper.findColors(getContext(), colorSuggestion.getBody(),
                        new DataHelper.OnFindColorsListener() {

                            @Override
                            public void onResults(List<ColorWrapper> results) {
                                //show search results
                            }

                        });
                Log.d("LOG", "onSuggestionClicked()");*/

                mLastQuery = searchSuggestion.getBody();
            }

            @Override
            public void onSearchAction(String query) {
                getPlacesbyIdCategory(query);
            }
        });

        mSearchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {

                //show suggestions when search bar gains focus (typically history suggestions)
                mSearchView.swapSuggestions(categoryRepository.getHistory(getContext(), 6));

                Log.d("LOG", "onFocus()");
            }

            @Override
            public void onFocusCleared() {

                //set the title of the bar so that when focus is returned a new query begins
                mSearchView.setSearchBarTitle(mLastQuery);
                Log.d("LOG", "onFocusCleared()");
            }
        });

//**
        //handle menu clicks the same way as you would
        //in a regular activity
        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {

                if (item.getItemId() == R.id.showCategories) {
                    navigateToCategoriesList();
                }

            }
        });

        //use this listener to listen to menu clicks when app:floatingSearch_leftAction="showHome"
        mSearchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {

                Log.d("LOG", "onHomeClicked()");
            }
        });

        /*
         * Here you have access to the left icon and the text of a given suggestion
         * item after as it is bound to the suggestion list. You can utilize this
         * callback to change some properties of the left icon and the text. For example, you
         * can load the left icon images using your favorite image loading library, or change text color.
         *
         *
         * Important:
         * Keep in mind that the suggestion list is a RecyclerView, so views are reused for different
         * items in the list.
         */
        mSearchView.setOnBindSuggestionCallback(new SearchSuggestionsAdapter.OnBindSuggestionCallback() {
            @Override
            public void onBindSuggestion(View suggestionView, ImageView leftIcon,
                                         TextView textView, SearchSuggestion item, int itemPosition) {
                CategorySuggestion colorSuggestion = (CategorySuggestion) item;
                String textColor = "#000000";
                String textLight = "#EC7063";
                /*if (colorSuggestion.getIsHistory()) {
                    leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.ic_history_black_24dp, null));
                    Util.setIconColor(leftIcon, Color.parseColor(textColor));
                    leftIcon.setAlpha(.36f);
                } else {*/
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.alpha_gradient);
                requestOptions.error(R.drawable.image_broken);
                requestOptions.fitCenter();
                requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
                Glide
                        .with(getContext())
                        .load(colorSuggestion.getIcon())
                        .apply(requestOptions)
                        .into(leftIcon);
                   /* leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                           colorSuggestion.getIcon(), null));*/

                leftIcon.setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);


                /*}*/

                textView.setTextColor(Color.parseColor(textColor));
                String text = colorSuggestion.getBody().toLowerCase()
                        .replaceFirst(mSearchView.getQuery(),
                                "<font color=\"" + textLight + "\">" + mSearchView.getQuery() + "</font>");
                textView.setText(Html.fromHtml(text));
            }

        });
    }

    private void navigateToPlaceFragment(String id_place) {

        Bundle bundle = new Bundle();
        bundle.putString("id_place", id_place);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PlaceFragment placeFragment = new PlaceFragment();
        placeFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, placeFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(placeFragment.getTag());
        fragmentTransaction.commit();

    }

    private void navigateToCategoriesList(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ListCategoriesFragment categoriesFragment = new ListCategoriesFragment();
        fragmentTransaction.replace(R.id.frame_layout, categoriesFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(categoriesFragment.getTag());
        fragmentTransaction.commit();
    }

    public void showProgressDialog(String msj) {
        progressDialog.setMessage(msj);
        progressDialog.show();
    }

    public void hideProgressDialog() {
        progressDialog.hide();
    }
}



