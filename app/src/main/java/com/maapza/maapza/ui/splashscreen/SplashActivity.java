package com.maapza.maapza.ui.splashscreen;

/**
 * Created by LUIS.RODARTE on 10/01/2018.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.maapza.maapza.ui.login.signin.PhoneSigninActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CountDownTimer(1000, 1000) {
            public void onFinish() {
                // Start home activity
                startActivity(new Intent(SplashActivity.this, PhoneSigninActivity.class));
                // close splash activity
                finish();
            }

            public void onTick(long millisUntilFinished) {
                // millisUntilFinished    The amount of time until finished.
            }
        }.start();

    }
}