package com.maapza.maapza.ui.login.signup;


public class SignupPresenterImpl implements SignupContract.Presenter, SignupContract.Interactor.OnFinishedListener {

    private SignupContract.View view;
    private SignupContract.Interactor interactor;


    public SignupPresenterImpl(SignupContract.View view) {
        this.view = view;
        interactor = new SignupInteractorImpl();
    }

    @Override
    public void registerUser(String email, String password, String username) {
        view.showLoading(true);
        interactor.registerUser(email, password, username, this);
    }

    @Override
    public void onFinished() {
        view.showLoading(false);
        view.showSuccessMessage();
        view.navToMainActivity();
        view.closeActivity();
    }

    @Override
    public void onFailure(String msg) {
        view.showLoading(false);
        view.showErrorMessage(msg);

    }

    @Override
    public void onUserAlreadyRegistered() {
        view.showLoading(false);
        view.showErrorUserRegisteredMessage();
    }
}
