package com.maapza.maapza.ui.login.signin;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.maapza.maapza.MainActivity;
import com.maapza.maapza.R;
import com.maapza.maapza.ui.login.resetpassword.ResetPassActivity;
import com.maapza.maapza.ui.login.signup.SignupActivity;

/**
 * Created by MacHD on 2/10/18.
 */

public class SigninActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnLogin;
    private FirebaseAuth mAuth;
    private EditText editTextEmail, editTextPassword;
    private TextView tvRegister, tvResetPass;
    private ProgressBar progressBar;
    private Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);
        findViewById(R.id.textViewSignup).setOnClickListener(this);
        findViewById(R.id.textViewForgotPass).setOnClickListener(this);
        findViewById(R.id.buttonLogin).setOnClickListener(this);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        tvRegister = findViewById(R.id.textViewSignup);
        tvResetPass = findViewById(R.id.textViewForgotPass);
        btnLogin= findViewById(R.id.buttonLogin);
        progressBar = findViewById(R.id.progressbar);
        mAuth = FirebaseAuth.getInstance();
        TranslateAnimation anim = new TranslateAnimation(0, 0, 190, 0);
        anim.setDuration(2500);
        anim.setFillAfter(true);
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.imgMaapza);
        splash.startAnimation(anim);
        font = Typeface.createFromAsset(this.getAssets(), "quicksand.ttf");
        editTextEmail.setTypeface(font);
        editTextPassword.setTypeface(font);
        tvRegister.setTypeface(font);
        tvResetPass.setTypeface(font);
    }

    public void userLogin() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (email.isEmpty()) {
            editTextEmail.setError("Email requerido");
            editTextEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Ingresa un email valido");
            editTextEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            editTextPassword.setError("Password requerida");
            editTextPassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            editTextPassword.setError("La contrasena debe ser mayor a 6 caracteres");
            editTextPassword.requestFocus();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()) {
                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser()!=null){
            finish();
            startActivity(new Intent(this,MainActivity.class));
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewSignup:
                finish();
                startActivity(new Intent(this, SignupActivity.class));
                break;
            case R.id.textViewForgotPass:
                finish();
                startActivity(new Intent(this, ResetPassActivity.class));
                break;
            case R.id.buttonLogin:
                userLogin();
                break;
        }

    }
}