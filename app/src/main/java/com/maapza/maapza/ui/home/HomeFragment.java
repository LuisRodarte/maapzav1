/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.maapza.maapza.ui.home;

import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.ui.categories.CategoriesAdapter;
import com.maapza.maapza.ui.categories.ListCategoriesFragment;
import com.maapza.maapza.ui.listplaces.ListPlacesFragment;
import com.maapza.maapza.ui.listplaces.PlaceAdapter;
import com.maapza.maapza.ui.map.MapFragment;
import com.maapza.maapza.ui.placedetail.PlaceFragment;

import java.util.List;

import static com.maapza.maapza.common.Constants.BUNDLE_ACTION;
import static com.maapza.maapza.common.Constants.BUNDLE_ACTION_CATEGORY;
import static com.maapza.maapza.common.Constants.BUNDLE_ID_CATEGORY;
import static com.maapza.maapza.common.Constants.BUNDLE_ID_PLACE;
import static com.maapza.maapza.common.Constants.BUNDLE_NAME_CATEGORY;

public class HomeFragment extends Fragment implements HomeContract.View, CategoriesAdapter.ClickHandler, PlaceAdapter.ClickHandler {

    private RecyclerView recyclerViewCateg;
    private View myFragmentView;
    private RecyclerView recyclerViewPlace;
    private ProgressBar progressBar;

    private HomeContract.Presenter presenter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (checkPermission()) {
        } else askPermission();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_home, container, false);
        presenter = new HomePresenterImpl(this);
        setUI();
        presenter.getListCategories();
        presenter.getPlaces();
        return myFragmentView;

    }

    private void setUI() {
        Typeface face;
        face = Typeface.createFromAsset(getContext().getAssets(), "quicksand.ttf");
        TextView mTextViewTitle = myFragmentView.findViewById(R.id.textTitle);
        TextView mTextViewSub = myFragmentView.findViewById(R.id.textSub);
        Button btnShowAll = myFragmentView.findViewById(R.id.btnShowAllPlaces);
        Button btnShowAllCat = myFragmentView.findViewById(R.id.btnAllCat);
        recyclerViewPlace = myFragmentView.findViewById(R.id.rviewDashboard);
        progressBar= myFragmentView.findViewById(R.id.progressbar);
        mTextViewTitle.setTypeface(face);
        mTextViewSub.setTypeface(face);
        btnShowAll.setTypeface(face);
        btnShowAllCat.setTypeface(face);

        //MENU LIST
        recyclerViewCateg = myFragmentView.findViewById(R.id.recycleViewCategories);
        LinearLayoutManager layoutManager = new LinearLayoutManager(myFragmentView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManager.scrollToPositionWithOffset(1, 200);
        recyclerViewCateg.setHasFixedSize(true);

        recyclerViewCateg.setLayoutManager(layoutManager);
        btnShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navToListPlaces();
            }
        });

        btnShowAllCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navToCategories();

            }
        });

        //PLACES LIST
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerViewPlace.setLayoutManager(gridLayoutManager);
        recyclerViewPlace.setHasFixedSize(true);
        recyclerViewPlace.setItemViewCacheSize(20);
        recyclerViewPlace.setDrawingCacheEnabled(true);
        recyclerViewPlace.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    private void navToCategories() {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ListCategoriesFragment categoriesFragment = new ListCategoriesFragment();
        fragmentTransaction.replace(R.id.frame_layout, categoriesFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(categoriesFragment.getTag());
        fragmentTransaction.commit();
    }

    private void navToListPlaces() {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ListPlacesFragment listPlacesFragment = new ListPlacesFragment();
        fragmentTransaction.replace(R.id.frame_layout, listPlacesFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(listPlacesFragment.getTag());
        fragmentTransaction.commit();
    }

    // Check for permission to access Location
    private boolean checkPermission() {
        // Ask for permission if it wasn't granted yet
        return (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // Asks for permission
    private void askPermission() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                1
        );
    }

    @Override
    public void showCategoryErrorMessage() {
        Toast.makeText(getContext(), getString(R.string.error_get_categories), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPlaceErrorMessage() {
        Toast.makeText(getContext(), getString(R.string.error_get_places), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        getActivity().finish();
    }

    @Override
    public void displayListCategories(List<Category> categories) {
        CategoriesAdapter adapterCateg = new CategoriesAdapter(getContext(), categories, 1, this);
        recyclerViewCateg.setAdapter(adapterCateg);
    }

    @Override
    public void displayListPlaces(List<Place> places) {
        PlaceAdapter adapterPlace = new PlaceAdapter(getContext(), places, "", this);
        recyclerViewPlace.setAdapter(adapterPlace);
    }

    @Override
    public void OnclickItem(String idCategory, String name) {

        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ID_CATEGORY, idCategory);
        bundle.putString(BUNDLE_NAME_CATEGORY, name);
        bundle.putString(BUNDLE_ACTION, BUNDLE_ACTION_CATEGORY);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, mapFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onClickPlace(String idPlace) {
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ID_PLACE, idPlace);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PlaceFragment placeFragment = new PlaceFragment();
        placeFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, placeFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(placeFragment.getTag());
        fragmentTransaction.commit();
    }
}
