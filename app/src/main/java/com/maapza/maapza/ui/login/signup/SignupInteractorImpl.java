package com.maapza.maapza.ui.login.signup;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.maapza.maapza.data.remote.model.User;

import static com.maapza.maapza.common.Constants.DEFAULTPHOTOURL;

public class SignupInteractorImpl implements SignupContract.Interactor {

    private FirebaseAuth auth;
    FirebaseDatabase database;
    private SignupInteractorImpl.OnFinishedListener onFinishedListener;

    public SignupInteractorImpl() {
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void registerUser(final String email, String password, final String username, final OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            DatabaseReference profile = database.getReference("users");
                            User newUser = new User(email, auth.getCurrentUser().getUid(), username, DEFAULTPHOTOURL);
                            profile.child(auth.getCurrentUser().getUid()).setValue(newUser);
                            onFinishedListener.onFinished();

                        } else {
                            //If user is already registered
                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                onFinishedListener.onUserAlreadyRegistered();
                            else onFinishedListener.onFailure(task.getException().getMessage());
                        }
                    }
                });
    }
}
