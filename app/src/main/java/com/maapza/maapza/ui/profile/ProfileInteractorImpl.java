package com.maapza.maapza.ui.profile;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.maapza.maapza.data.remote.model.User;

public class ProfileInteractorImpl implements ProfileContract.Interactor {

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    public ProfileInteractorImpl() {
        database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void doGetUserInfo(final OnFinishedListener onFinishedListener) {
        final FirebaseUser firebaseUser = mAuth.getCurrentUser();
        DatabaseReference profile = database.getReference("users").child(firebaseUser.getUid());
        profile.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    onFinishedListener.onFinishedGetUserInfo(user, firebaseUser.getPhotoUrl());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFinishedListener.onErrorGetUserInfo();

            }

        });

    }

    @Override
    public void doGetSession(final OnFinishedListener onFinishedListener) {

        onFinishedListener.onFinishedGetSession(mAuth.getCurrentUser() != null);

    }

    @Override
    public void doSignOut(final OnFinishedListener onFinishedListener) {

    }

    @Override
    public void doUpdateName(final OnFinishedListener onFinishedListener, String name) {

        FirebaseUser user = mAuth.getCurrentUser();
        DatabaseReference profile = database.getReference("users").child(user.getUid());
        profile.child("name_user").setValue(name);
        profile.child("photo_url").setValue(user.getPhotoUrl().toString());
        UserProfileChangeRequest profileRequest = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        user.updateProfile(profileRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    onFinishedListener.onFinishedUpdateUser();
                else onFinishedListener.onErrorUpdateUser();
            }
        });


    }

    @Override
    public void doUpdatePhoto(final OnFinishedListener onFinishedListener, Uri uriProfileImage) {
        StorageReference profileImageRef = FirebaseStorage.getInstance().getReference("profileimages/" + mAuth.getCurrentUser().getUid() + ".jpg");
        if (uriProfileImage != null) {
            profileImageRef.putFile(uriProfileImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setPhotoUri(taskSnapshot.getDownloadUrl())
                            .build();

                    user.updateProfile(profileUpdates)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        onFinishedListener.onFinishedUpdatePhoto();
                                    } else onFinishedListener.onErrorUpdatePhoto();

                                }
                            });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    onFinishedListener.onErrorUpdatePhoto();
                }
            });

        }
    }
}
