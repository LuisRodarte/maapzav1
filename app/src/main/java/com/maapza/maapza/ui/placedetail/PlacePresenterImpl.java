package com.maapza.maapza.ui.placedetail;

import com.maapza.maapza.data.remote.model.Comment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.data.remote.model.PlacePhotos;
import com.maapza.maapza.data.remote.model.Times;

import java.util.List;

public class PlacePresenterImpl implements PlaceContract.Presenter, PlaceContract.Interactor.OnFinishedListener {

    private PlaceContract.View view;
    private PlaceContract.Interactor interactor;

    public PlacePresenterImpl(PlaceContract.View view) {
        this.view = view;
        interactor = new PlaceInteractorImpl();
    }

    @Override
    public void getPlacesbyId(String place_id) {

    }

    @Override
    public void getPhotosbyIdPlace(String place_id) {

    }

    @Override
    public void getTimesByIdPlace(String place_id) {

    }

    @Override
    public void getCommentsByIdPlace(String place_id) {

    }

    @Override
    public void onFinishedGetPlaces(Place place) {

    }

    @Override
    public void onFinishedGetPhotos(List<PlacePhotos> listPhotos) {

    }

    @Override
    public void onFinishedGetTimes(List<Times> timesList) {

    }

    @Override
    public void onFinishedGetComments(List<Comment> listComments) {

    }

    @Override
    public void onErrorGetData() {

    }
}
