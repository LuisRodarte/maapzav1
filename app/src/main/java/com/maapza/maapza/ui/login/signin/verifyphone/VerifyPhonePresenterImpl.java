package com.maapza.maapza.ui.login.signin.verifyphone;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.maapza.maapza.R;

public class VerifyPhonePresenterImpl implements VerifyPhoneContract.Presenter, VerifyPhoneContract.Interactor.OnFinishedListener {

    private VerifyPhoneContract.View view;
    private VerifyPhoneContract.Interactor interactor;


    public VerifyPhonePresenterImpl(VerifyPhoneContract.View view) {
        this.view = view;
        interactor = new VerifyPhoneInteractorImpl();
    }

    @Override
    public void verifyPhoneNumber(Activity activity,String phoneNumber, String code) {
        view.showLoading(true);
        interactor.verifyPhoneNumber(activity,phoneNumber, code, this);
    }

    @Override
    public void onVerifySuccess() {
        view.showLoading(false);
        view.navToMainActivity();
    }

    @Override
    public void onVerifyError() {
        view.showLoading(false);
        view.showErrorMessage();

    }

    @Override
    public void sendCode(String code) {
        view.showLoading(false);
        view.completeCode(code);
    }
}
