

package com.maapza.maapza.ui.placedetail;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DatabaseReference;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Comment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.data.remote.model.PlacePhotos;
import com.maapza.maapza.data.remote.model.Times;
import com.maapza.maapza.ui.map.MapFragment;
import com.maapza.maapza.ui.placedetail.comments.CommentsAdapter;
import com.maapza.maapza.utils.DateUtils;
import com.maapza.maapza.utils.GPSTracker;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.Collections;
import java.util.List;

import static com.maapza.maapza.common.Constants.BUNDLE_ACTION;
import static com.maapza.maapza.common.Constants.BUNDLE_ACTION_PLACE;
import static com.maapza.maapza.common.Constants.BUNDLE_ID_PLACE;
import static com.maapza.maapza.common.Constants.BUNDLE_LATITUDE;
import static com.maapza.maapza.common.Constants.BUNDLE_LONGITUDE;


public class PlaceFragment extends Fragment implements View.OnClickListener, PlaceContract.View, CommentsAdapter.ClickHandler {
    private View myFragmentView;
    private String place_name;
    private String place_id;
    private Double lat;
    private Double lng;
    private TextView tvNamePlace, tvPhone, tvAddress, tvPlaceDescription, tvTimes, tvPrice;
    private ProgressBar progressBar;

    private ImageView imgAddress, imgPhone;
    private ToggleButton ivShowTimes;
    private FloatingActionButton fabNavigate;
    RatingBar ratingPlace;
    private Button btnLeaveRate;
    private Context mContext;
    CarouselView carouselView;
    LocationManager locationManager;
    private static final int REQUEST_LOCATION = 1;
    private Double latitude, longitude;
    private String currentLocation = "";
    private GPSTracker gps;
    private DateUtils date = new DateUtils();
    private CommentsAdapter mAdapter;
    private TimesAdapter mTimesAdapter;
    private RecyclerView recyclerViewComments, recyclerViewTimes;
    private Typeface font;
    private PlaceContract.Presenter presenter;

    public static PlaceFragment newInstance() {
        PlaceFragment fragment = new PlaceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_place, container, false);
        mContext = container.getContext();
        presenter = new PlacePresenterImpl(this);
        setUI();


        return myFragmentView;

    }

    private void setUI() {
        Bundle bundle = getArguments();
        if (bundle != null)
            place_id = bundle.getString(BUNDLE_ID_PLACE);
        tvNamePlace = myFragmentView.findViewById(R.id.tvPlaceName);
        imgAddress = myFragmentView.findViewById(R.id.imgAddress);
        imgPhone = myFragmentView.findViewById(R.id.imgPhone);
        fabNavigate = myFragmentView.findViewById(R.id.fabNavigate);
        ratingPlace = myFragmentView.findViewById(R.id.place_rating);
        btnLeaveRate = myFragmentView.findViewById(R.id.leaveRate);
        tvPhone = myFragmentView.findViewById(R.id.txtPhone);
        tvAddress = myFragmentView.findViewById(R.id.txtAdress);
        tvPlaceDescription = myFragmentView.findViewById(R.id.tvPlaceDescription);
        tvTimes = myFragmentView.findViewById(R.id.txtTimes);
        tvPrice = myFragmentView.findViewById(R.id.txtPrice);
        carouselView = myFragmentView.findViewById(R.id.carouselView);
        recyclerViewComments = myFragmentView.findViewById(R.id.recycleViewComments);
        recyclerViewTimes = myFragmentView.findViewById(R.id.recyclerViewTimes);
        ivShowTimes = myFragmentView.findViewById(R.id.ivShowTimes);
        progressBar = myFragmentView.findViewById(R.id.progressbar);
        font = Typeface.createFromAsset(mContext.getAssets(), "quicksand.ttf");
        tvNamePlace.setTypeface(font);
        tvPhone.setTypeface(font);
        tvAddress.setTypeface(font);
        tvPlaceDescription.setTypeface(font);
        tvTimes.setTypeface(font);
        btnLeaveRate.setTypeface(font);
        imgPhone.setOnClickListener(this);
        imgAddress.setOnClickListener(this);
        tvPhone.setOnClickListener(this);
        tvAddress.setOnClickListener(this);
        fabNavigate.setOnClickListener(this);
        btnLeaveRate.setOnClickListener(this);
        tvTimes.setOnClickListener(this);
        if (!place_id.isEmpty()) {
            presenter.getPlacesbyId(place_id);
            presenter.getPhotosbyIdPlace(place_id);
            presenter.getTimesByIdPlace(place_id);
            presenter.getCommentsByIdPlace(place_id);

        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewComments.setLayoutManager(mLayoutManager);
        recyclerViewComments.setItemAnimator(new DefaultItemAnimator());
        recyclerViewComments.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManagerTimes = new LinearLayoutManager(getContext());
        recyclerViewTimes.setLayoutManager(mLayoutManagerTimes);
        recyclerViewTimes.setItemAnimator(new DefaultItemAnimator());
        recyclerViewTimes.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

        ivShowTimes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) recyclerViewTimes.setVisibility(View.VISIBLE);
                else recyclerViewTimes.setVisibility(View.GONE);

            }
        });
    }


    public void onDestroyView() {

        super.onDestroyView();
        carouselView.destroyDrawingCache();

    }

    //send phone number to call app
    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
            startActivity(intent);
        }


    }

    //Navigate to current location to place selected
    public void navigatetoPlace(String currentLoc, String coordFinish) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + currentLoc + "&daddr=" + coordFinish));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(mContext, getString(R.string.no_map_app), Toast.LENGTH_LONG).show();
        }

    }

    public String getCoords() {
        //GPS
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            // Toast.makeText(this, "You need have granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(mContext, getActivity());
            // Check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }

        return latitude + "," + longitude;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    currentLocation = getCoords();
                } else {
                    // Can't get location.
                    // GPS or network is not enabled.
                    // Ask user to enable GPS/network in settings.
                    //gps.showSettingsAlert();
                }
            }
            return;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgPhone:
            case R.id.txtPhone:
                if (tvPhone.getText() != null && !tvPhone.getText().equals("")) {
                    //Send phone number to call
                    dialPhoneNumber(tvPhone.getText().toString().trim());
                }
                break;
            case R.id.imgAddress:

            case R.id.txtAdress:
                navToMap(place_id, lat, lng);
                break;
            case R.id.fabNavigate:
                currentLocation = getCoords();
                if (!currentLocation.equals("") && !currentLocation.isEmpty() && !currentLocation.equals("null,null"))
                    navigatetoPlace(currentLocation, lat.toString() + "," + lng.toString());
                else
                    Toast.makeText(mContext, getString(R.string.cant_get_current_location), Toast.LENGTH_SHORT).show();
                break;
            case R.id.leaveRate:
                ShowAlertDialogReview();
                break;
            case R.id.txtTimes:
                ivShowTimes.performClick();
                break;
        }
    }


    public void navToMap(String place_id, Double lat, Double lng) {
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ID_PLACE, place_id);
        bundle.putDouble(BUNDLE_LATITUDE, lat);
        bundle.putDouble(BUNDLE_LONGITUDE, lng);
        bundle.putString(BUNDLE_ACTION, BUNDLE_ACTION_PLACE);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, mapFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    // do something with the data coming from the AlertDialog
    private void ShowAlertDialogReview() {

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.activity_review, null);
        builder.setView(customLayout);
        final EditText review = customLayout.findViewById(R.id.editTextReview);
        final RatingBar ratingbar = customLayout.findViewById(R.id.ratingBar);
        final TextView txtOpinion = customLayout.findViewById(R.id.tvOpinion);
        review.setTypeface(font);
        txtOpinion.setTypeface(font);
        Button submit = customLayout.findViewById(R.id.send);
        submit.setTypeface(font);
        ImageButton cancel = customLayout.findViewById(R.id.cancel);
        // create and show the alert dialog
        final AlertDialog dialog = builder.create();
        // add a button
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(review.getText().toString())) {
                    review.requestFocus();
                }
                if (ratingbar.getRating() < 1f) {
                    Toast.makeText(getContext(), getResources().getString(R.string.alertReview), Toast.LENGTH_SHORT).show();
                    ratingbar.requestFocus();
                } else {
                    sendReview(review.getText().toString(), ratingbar.getRating(), date.getCurrentDate());
                    dialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void sendReview(String comments, Float rating, String date) {
        if (TextUtils.isEmpty(comments)) {
            comments = getResources().getString(R.string.noComments);
        }
        presenter.sendReview(place_id, comments, rating, date);
    }


    @Override
    public void showErrorMessage() {

        Toast.makeText(mContext, getString(R.string.error_get_data), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        getActivity().finish();

    }

    @Override
    public void displayPlaceData(Place place) {

        if (place != null) {
            tvNamePlace.setText(place.getName());
            tvPlaceDescription.setText(place.getDescription());
            tvAddress.setText(place.getAddress());
            tvPhone.setText(place.getPhone());
            if (place.getPrice_min() != null && place.getPrice_max() != null)
                tvPrice.setText(getString(R.string.prices, place.getPrice_min(), place.getPrice_max()));
            else
                tvPrice.setText(getString(R.string.prices, 0.0f, 0.0f));
            ratingPlace.setRating(place.getAverage_rating());
            place_name = place.getName();
            lat = place.getLatitude();
            lng = place.getLongitude();
        }

        ViewCompat.setTransitionName(myFragmentView.findViewById(R.id.app_bar_layout), place_name);

    }

    @Override
    public void displayTimes(List<Times> timesList) {
        final int currentDay = DateUtils.getCurrentDay();
        for (Times time : timesList) {
            if (time.getId_time() == currentDay) {
                if (DateUtils.isAvailable(time.getTime_open(), time.getTime_closed()))
                    tvTimes.setText(getString(R.string.current_times, time.getTime_open(), time.getTime_closed(), getString(R.string.open)));
                else
                    tvTimes.setText(getString(R.string.current_times, time.getTime_open(), time.getTime_closed(), getString(R.string.closed)));
            }
        }

        mTimesAdapter = new TimesAdapter(getContext(), timesList);
        recyclerViewTimes.setAdapter(mTimesAdapter);
    }

    @Override
    public void displayComments(List<Comment> listComments) {
        Collections.reverse(listComments);
        mAdapter = new CommentsAdapter(getContext(), listComments, this);
        recyclerViewComments.setAdapter(mAdapter);
    }

    @Override
    public void displayPhotos(final List<PlacePhotos> listPhotos) {

        if (!listPhotos.isEmpty()) {
            ImageListener imageListener = new ImageListener() {
                @Override
                public void setImageForPosition(int position, ImageView imageView) {

                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.alpha_gradient);
                    requestOptions.error(R.drawable.image_broken);
                    requestOptions.centerCrop();
                    requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
                    requestOptions.override(500, 300);
                    if (position < listPhotos.size()) {
                        Glide.with(myFragmentView.getContext())
                                .load(listPhotos.get(position).getUrl())
                                .apply(requestOptions)
                                .into(imageView);
                    }

                }
            };
            carouselView.setImageListener(imageListener);
            carouselView.setPageCount(listPhotos.size());
        }

    }

    @Override
    public void onClickComment(Comment comment) {
        // create an alert builder
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        // set the custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.comment_list_row, null);
        builder.setView(customLayout);
        TextView commentComplete = customLayout.findViewById(R.id.textComment);
        TextView user = customLayout.findViewById(R.id.txtName);
        TextView txtDate = customLayout.findViewById(R.id.textDate);
        ImageView photo = customLayout.findViewById(R.id.imageUser);
        RatingBar ratingbar = customLayout.findViewById(R.id.ratingBar);
        user.setText(comment.getName_user());
        txtDate.setText(comment.getComment_date());
        commentComplete.setText(comment.getComment());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.alpha_gradient);
        requestOptions.error(R.drawable.image_broken);
        requestOptions.centerCrop();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        requestOptions.override(150, 150);
        Glide.with(this)
                .load(comment.getUrl_photo())
                .apply(requestOptions)
                .into(photo);

        ratingbar.setRating(comment.getRating());

        // add a button
        builder.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // send data from the AlertDialog to the Activity

                //sendDialogDataToActivity(editText.getText().toString());
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

