package com.maapza.maapza.ui.categories;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Category;

import java.util.List;


/**
 * Created by MacHD on 1/23/18.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private Context mContext;
    private List<Category> categoriesList;
    private ClickHandler  clickHandler;
    private Typeface face;
    private int type;
    private String[] colors = {"card_rounded"
            , "card_rounded2",
            "card_rounded3",
            "card_rounded4",
            "card_rounded5",
            "card_rounded6",
            "card_rounded7",
            "card_rounded8",
            "card_rounded9",
            "card_rounded10"};
    private int index = -1;


    public CategoriesAdapter(Context mContext, List<Category> categoriesList, int type,ClickHandler  clickHandler) {
        this.mContext = mContext;
        this.categoriesList = categoriesList;
        this.type = type;
        this.clickHandler=clickHandler;
        face = Typeface.createFromAsset(mContext.getAssets(), "quicksand.ttf");
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView;
        if (type == 1)
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        else
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_categories, parent, false);

        return new CategoriesViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, int position) {

        Category categories = categoriesList.get(position);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.alpha_gradient);
        requestOptions.error(R.drawable.image_broken);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        Glide.with(mContext)
                .load(categories.getIcon())
                .apply(requestOptions)
                .into(holder.iconCategory);
        holder.nameCategory.setText(categories.getName());
        holder.nameCategory.setTypeface(face);
        index++;
        int drawableResourceId = mContext.getResources().getIdentifier(colors[0], "drawable", mContext.getPackageName());
        if (index > colors.length - 1) {
            index = 0;
            drawableResourceId = mContext.getResources().getIdentifier(colors[0], "drawable", mContext.getPackageName());
        } else
            drawableResourceId = mContext.getResources().getIdentifier(colors[index], "drawable", mContext.getPackageName());
        holder.imgButton.setBackground(ContextCompat.getDrawable(mContext, drawableResourceId));
    }


    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    class CategoriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView iconCategory;
        TextView nameCategory;
        ImageButton imgButton;


        public CategoriesViewHolder(View itemView) {
            super(itemView);
            iconCategory = itemView.findViewById(R.id.icon_categ);
            nameCategory = itemView.findViewById(R.id.title_categ);
            imgButton = itemView.findViewById(R.id.list_container_bg);
            imgButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.list_container_bg) {
                if (clickHandler != null) {
                    clickHandler.OnclickItem(categoriesList.get(getAdapterPosition()).getId_category(), categoriesList.get(getAdapterPosition()).getId_category());
                }
            }
        }
    }

    public interface ClickHandler{
        void OnclickItem(String idCategory,String name);
    }
}
