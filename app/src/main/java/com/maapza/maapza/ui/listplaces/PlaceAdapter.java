package com.maapza.maapza.ui.listplaces;

import android.content.Context;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Place;

import java.util.List;


/**
 * Created by MacHD on 1/15/18.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> {
    private Context mcontext;
    private List<Place> placesList;
    private String type;
    private ClickHandler clickHandler;

    public PlaceAdapter(Context mcontext, List<Place> placesList, String type, ClickHandler clickHandler) {
        this.mcontext = mcontext;
        this.placesList = placesList;
        this.type = type;
        this.clickHandler=clickHandler;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_places, parent, false);
        if (type.equals("all")) {
            layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_all_places, parent, false);
        }

        PlaceViewHolder holder = new PlaceViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        Place place = placesList.get(position);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.alpha_gradient);
        requestOptions.error(R.drawable.image_broken);
        requestOptions.centerCrop();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        requestOptions .override(250, 150);
        Glide.with(mcontext)
                .load(place.getMain_photo())
                .apply(requestOptions)
                .into(holder.photo);
        Typeface face;
        face = Typeface.createFromAsset(mcontext.getAssets(), "quicksand.ttf");
        holder.name.setTypeface(face);
        holder.description.setTypeface(face);
        holder.name.setText(place.getName());
        holder.description.setText(place.getDescription());
        holder.rating.setRating(place.getAverage_rating());


    }

    @Override
    public int getItemCount() {
        return placesList.size();
    }

    class PlaceViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView name;
        TextView description;
        RatingBar rating;
        ConstraintLayout container;


        public PlaceViewHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.place_photo);
            name = itemView.findViewById(R.id.place_name);
            description = itemView.findViewById(R.id.place_description);
            rating = itemView.findViewById(R.id.place_rating);
            container=itemView.findViewById(R.id.container);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickHandler.onClickPlace(placesList.get(getAdapterPosition()).getId_place());
                }
            });
        }
    }

    public interface ClickHandler{
        void onClickPlace(String idPlace);
    }
}
