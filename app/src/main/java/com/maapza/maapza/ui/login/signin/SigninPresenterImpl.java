package com.maapza.maapza.ui.login.signin;


import com.google.firebase.auth.FirebaseUser;


public class SigninPresenterImpl implements SigninContract.Presenter, SigninContract.Interactor.OnFinishedListener {

    private SigninContract.View view;
    private SigninContract.Interactor interactor;

    public SigninPresenterImpl(SigninContract.View view) {
        this.view = view;
        interactor = new SigninIteractorImpl();
    }

    @Override
    public void checkSession() {
        view.showLoading(true);
        interactor.checkSession(this);
    }

    @Override
    public void onFinished(FirebaseUser user) {
        view.showLoading(false);
        if (user != null) view.navToMainActivity();
    }

    @Override
    public void onFailure(Throwable t) {
        view.showLoading(false);
        view.showErrorMessage();
    }
}
