
package com.maapza.maapza.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.User;
import com.maapza.maapza.ui.login.signin.PhoneSigninActivity;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment implements ProfileContract.View {
    private static final int CHOSE_IMAGE = 101;
    private View myFragmentView;
    private EditText editTextName;
    private Context mContext;
    private CircularImageView circularImageView;
    private ProgressBar progressBar;
    private Button btnSave;
    private Button btnEditName;
    private ProfileContract.Presenter presenter;


    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_profile, container, false);
        presenter = new ProfilePresenterImpl(this);
        setUI();
        presenter.validateSession();
        return myFragmentView;

    }

    private void setUI() {
        circularImageView = myFragmentView.findViewById(R.id.img_profile);
        editTextName = myFragmentView.findViewById(R.id.txtname);
        TextView txtimage = myFragmentView.findViewById(R.id.txtimage);
        TextView txtSingout = myFragmentView.findViewById(R.id.txtSingout);
        progressBar = myFragmentView.findViewById(R.id.progressbar);
        btnSave = myFragmentView.findViewById(R.id.buttonSave);
        Button btnSingOut = myFragmentView.findViewById(R.id.buttonSingout);
        btnEditName = myFragmentView.findViewById(R.id.btnEditName);
        mContext = getContext();
        Typeface face = Typeface.createFromAsset(mContext.getAssets(), "quicksand.ttf");
        editTextName.setTypeface(face);
        editTextName.setEnabled(false);
        txtimage.setTypeface(face);
        btnSave.setTypeface(face);
        txtSingout.setTypeface(face);
        circularImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChoser();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserInformation();
            }
        });
        txtSingout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.signOut();
            }
        });
        btnSingOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.signOut();
            }
        });
        btnEditName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                editTextName.setEnabled(true);
                editTextName.requestFocus();
                btnEditName.setVisibility(View.GONE);
                btnSave.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOSE_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uriProfileImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getApplicationContext().getContentResolver(), uriProfileImage);
                circularImageView.setImageBitmap(bitmap);
                presenter.updateUserPhoto(uriProfileImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void saveUserInformation() {
        String name = editTextName.getText().toString();
        if (name.isEmpty()) {
            editTextName.setError(getString(R.string.enter_name));
            editTextName.requestFocus();
            return;
        }
        presenter.updateUserName(name);
    }

    private void showImageChoser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_profile_photo)), CHOSE_IMAGE);
    }

    @Override
    public void showErrorGetUserinfo() {
        Toast.makeText(mContext, getString(R.string.error_get_profile_data), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorUpdatingUserinfo() {
        Toast.makeText(mContext, getString(R.string.error_updating_profile), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        getActivity().finish();

    }

    @Override
    public void displayUserInfo(User user, Uri photoUrl) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.alpha_gradient);
        requestOptions.error(R.drawable.alpha_gradient4);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        requestOptions.override(250, 250);
        Glide.with(mContext)
                .load(photoUrl)
                .apply(requestOptions)
                .into(circularImageView);
        editTextName.setText(user.getName_user());
    }

    @Override
    public void showMessageUpdateSuccess() {
        Toast.makeText(mContext, getString(R.string.profile_updated), Toast.LENGTH_SHORT).show();
        editTextName.setEnabled(false);
        btnEditName.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.GONE);
    }

    @Override
    public void showErrorUpdatingPhoto() {
        Toast.makeText(mContext, getString(R.string.error_updating_profile), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessageUpdatingPhotoSuccess() {
        Toast.makeText(mContext, getString(R.string.photo_updated), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navToLogin() {
        getActivity().finish();
        startActivity(new Intent(mContext, PhoneSigninActivity.class));
    }
}

