package com.maapza.maapza.ui.login.resetpassword;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPassInteractorImpl implements ResetPassContract.Interactor {
    private FirebaseAuth mAuth;

    public ResetPassInteractorImpl() {
        mAuth = FirebaseAuth.getInstance();
    }
    @Override
    public void doResetPass(final OnFinishedListener onFinishedListener, String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                            onFinishedListener.onFinishedResetPass();
                        else
                            onFinishedListener.onErrorResetPass();
                    }
                });
    }
}
