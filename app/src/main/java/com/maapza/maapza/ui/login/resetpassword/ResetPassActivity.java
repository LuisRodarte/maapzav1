package com.maapza.maapza.ui.login.resetpassword;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.maapza.maapza.R;
import com.maapza.maapza.ui.login.signin.SigninActivity;

/**
 * Created by MacHD on 11/17/18.
 */

public class ResetPassActivity extends AppCompatActivity implements ResetPassContract.View {

    private EditText edtEmail;
    private ResetPassContract.Presenter presenter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        presenter = new ResetPassPresenterImpl(this);
        setUI();

    }

    private void setUI() {

        edtEmail = findViewById(R.id.edt_reset_email);
        progressBar= findViewById(R.id.progressbar);
        Button btnResetPassword = findViewById(R.id.btn_reset_password);
        TextView btnBack = findViewById(R.id.btn_back);
        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvMessage = findViewById(R.id.tvMessage);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "quicksand.ttf");
        edtEmail.setTypeface(font);
        btnBack.setTypeface(font);
        tvTitle.setTypeface(font);
        tvMessage.setTypeface(font);

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = edtEmail.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    edtEmail.setError(getApplicationContext().getString(R.string.enter_email));
                    return;
                }
                presenter.resetPassword(email);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
                startActivity(new Intent(getApplicationContext(), SigninActivity.class));
            }
        });
    }

    @Override
    public void showResetErrorMessage() {
        Toast.makeText(ResetPassActivity.this, R.string.failed_email, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public void resetSuccess() {
        Toast.makeText(ResetPassActivity.this, R.string.check_email, Toast.LENGTH_SHORT).show();

    }
}
