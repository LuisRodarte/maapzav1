package com.maapza.maapza.ui.placedetail;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Times;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LUIS.RODARTE on 22/05/2018.
 */

public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.MyViewHolder> {

    private List<Times> timesLists;
    Typeface face;
    private Context mContext;



    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDayName)
        TextView tvDayName;
        @BindView(R.id.tvTime)
        TextView tvTime;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }


    public TimesAdapter(Context mContext, List<Times> timesList) {
        this.mContext = mContext;
        this.timesLists = timesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_times, parent, false);
        face = Typeface.createFromAsset(mContext.getAssets(), "quicksand.ttf");
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Times time = timesLists.get(position);
        holder.tvDayName.setTypeface(face);
        holder.tvTime.setTypeface(face);

        holder.tvDayName.setText(time.getTime_name());
        holder.tvTime.setText(mContext.getString(R.string.times,time.getTime_open(),time.getTime_closed()));

    }

    @Override
    public int getItemCount() {
        return timesLists.size();
    }
}
