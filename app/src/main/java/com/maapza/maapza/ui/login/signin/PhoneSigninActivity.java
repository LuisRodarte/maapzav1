package com.maapza.maapza.ui.login.signin;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.maapza.maapza.MainActivity;
import com.maapza.maapza.R;
import com.maapza.maapza.ui.login.signin.verifyphone.VerifyPhoneActivity;

import static com.maapza.maapza.common.Constants.BUNDLE_PHONE_NUMBER;

/**
 * Created by LUIS.RODARTE on 2/10/18.
 */

public class PhoneSigninActivity extends AppCompatActivity implements SigninContract.View {

    private EditText editTextMobile;
    private Typeface font;
    private Button buttonContinue;
    private ProgressBar progressBar;
    private SigninContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin_phone);
        presenter = new SigninPresenterImpl(this);
        setUI();
        presenter.checkSession();
    }

    private void setUI() {

        editTextMobile = findViewById(R.id.editTextMobile);
        buttonContinue = findViewById(R.id.buttonContinue);
        progressBar= findViewById(R.id.progressbar);
        font = Typeface.createFromAsset(this.getAssets(), "quicksand.ttf");
        editTextMobile.setTypeface(font);
        buttonContinue.setTypeface(font);
        TranslateAnimation anim = new TranslateAnimation(0, 0, 0, 120);
        anim.setDuration(2500);
        anim.setFillAfter(true);
        // Start animating the image
        final ImageView splash = findViewById(R.id.imgPerson);
        splash.startAnimation(anim);
        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phone = editTextMobile.getText().toString().trim();
                if (phone.isEmpty() || phone.length() < 10) {
                    editTextMobile.setError(getApplicationContext().getString(R.string.valid_phone));
                    editTextMobile.requestFocus();
                    return;
                }
                navToVerifyPhoneActivity(phone);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, getString(R.string.sesion_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public void navToMainActivity() {
        closeActivity();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void navToVerifyPhoneActivity(String phone) {
        Intent intent = new Intent(PhoneSigninActivity.this, VerifyPhoneActivity.class);
        intent.putExtra(BUNDLE_PHONE_NUMBER, phone);
        startActivity(intent);
    }

}