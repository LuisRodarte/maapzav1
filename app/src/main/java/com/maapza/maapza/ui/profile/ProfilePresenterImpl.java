package com.maapza.maapza.ui.profile;

import android.net.Uri;

import com.maapza.maapza.data.remote.model.User;

public class ProfilePresenterImpl implements ProfileContract.Presenter, ProfileContract.Interactor.OnFinishedListener {

    private ProfileContract.View view;
    private ProfileContract.Interactor interactor;

    public ProfilePresenterImpl(ProfileContract.View view) {
        this.view = view;
        interactor = new ProfileInteractorImpl();
    }

    @Override
    public void getUserInfo() {
        view.showLoading(true);
        interactor.doGetUserInfo(this);

    }

    @Override
    public void validateSession() {
        view.showLoading(true);
        interactor.doGetSession(this);
    }

    @Override
    public void signOut() {
        view.showLoading(true);
        interactor.doSignOut(this);
    }

    @Override
    public void updateUserName(String name) {
        view.showLoading(true);
        interactor.doUpdateName(this, name);
    }

    @Override
    public void updateUserPhoto(Uri uriProfileImage) {
        view.showLoading(true);
        interactor.doUpdatePhoto(this, uriProfileImage);
    }

    @Override
    public void onFinishedSignout() {
        view.showLoading(false);
        view.closeActivity();

    }

    @Override
    public void onFinishedGetSession(Boolean session) {
        view.showLoading(false);
        if (session) interactor.doGetUserInfo(this);
        else view.navToLogin();

    }

    @Override
    public void onFinishedGetUserInfo(User user, Uri photoUrl) {
        view.showLoading(false);
        view.displayUserInfo(user, photoUrl);
    }

    @Override
    public void onErrorGetUserInfo() {
        view.showLoading(false);
        view.showErrorGetUserinfo();

    }

    @Override
    public void onFinishedUpdateUser() {
        view.showLoading(false);
        view.showMessageUpdateSuccess();
        interactor.doGetUserInfo(this);
    }

    @Override
    public void onErrorUpdateUser() {
        view.showLoading(false);
        view.showErrorUpdatingUserinfo();
    }

    @Override
    public void onFinishedUpdatePhoto() {
        view.showLoading(false);
        view.showMessageUpdatingPhotoSuccess();

    }

    @Override
    public void onErrorUpdatePhoto() {
        view.showLoading(false);
        view.showErrorUpdatingPhoto();

    }

    @Override
    public void onFailure(Throwable t) {

    }
}
