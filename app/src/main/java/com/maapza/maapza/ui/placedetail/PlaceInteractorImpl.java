package com.maapza.maapza.ui.placedetail;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Comment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.data.remote.model.PlacePhotos;
import com.maapza.maapza.data.remote.model.Times;
import com.maapza.maapza.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_ENDPOINT;
import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_IMAGES_ENDPOINT;
import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_RATES_ENDPOINT;
import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_REVIEWS_ENDPOINT;
import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_TIMES_ENDPOINT;

public class PlaceInteractorImpl implements PlaceContract.Interactor {

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;

    public PlaceInteractorImpl() {
        database = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void doGetPlacesbyId(final PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id) {

        DatabaseReference place = database.getReference(PLACE_ENDPOINT);
        Query query = place.orderByChild("id_place").equalTo(place_id);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        Place place = categorySnapshot.getValue(Place.class);
                        onFinishedListener.onFinishedGetPlaces(place);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFinishedListener.onErrorGetData();
            }
        });

    }

    @Override
    public void doGetPhotosbyIdPlace(final PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id) {

        DatabaseReference place_photos = database.getReference(PLACE_IMAGES_ENDPOINT).child(place_id);
        final List<PlacePhotos> listPhotos = new ArrayList<>();

        place_photos.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        PlacePhotos photos = categorySnapshot.getValue(PlacePhotos.class);
                        listPhotos.add(photos);
                    }
                    onFinishedListener.onFinishedGetPhotos(listPhotos);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFinishedListener.onErrorGetData();
            }
        });
    }

    @Override
    public void doGetTimesByIdPlace(final PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id) {
        final List<Times> listTimes = new ArrayList<>();
        DatabaseReference times = database.getReference(PLACE_TIMES_ENDPOINT).child(place_id);
        times.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        Times time = categorySnapshot.getValue(Times.class);
                        if (time != null) {
                            listTimes.add(time);
                        }
                    }
                }
                onFinishedListener.onFinishedGetTimes(listTimes);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFinishedListener.onErrorGetData();
            }
        });
    }

    @Override
    public void doGetCommentsByIdPlace(final PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id) {
        final List<Comment> commentsList = new ArrayList<>();
        DatabaseReference review = database.getReference(PLACE_REVIEWS_ENDPOINT).child(place_id);
        review.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    commentsList.clear();
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        Comment comments = categorySnapshot.getValue(Comment.class);
                        commentsList.add(comments);
                    }
                    onFinishedListener.onFinishedGetComments(commentsList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFinishedListener.onErrorGetData();
            }
        });
    }

    @Override
    public void doSendReview(final PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id, String comments, Float rating, String date) {

        mAuth.getCurrentUser();
        DatabaseReference review = database.getReference(PLACE_REVIEWS_ENDPOINT);
        DatabaseReference rate = database.getReference(PLACE_RATES_ENDPOINT);
        String reviewId = mAuth.getCurrentUser().getUid();
        String rateId = mAuth.getCurrentUser().getUid();

        if (mAuth.getCurrentUser().getPhotoUrl() != null) {
            Comment comment = new Comment(reviewId, mAuth.getCurrentUser().getUid(), mAuth.getCurrentUser().getDisplayName(), mAuth.getCurrentUser().getPhotoUrl().toString(), rating, comments, date, Integer.parseInt(place_id));
            review.child(place_id).child(reviewId).setValue(comment);
        } else {
            Comment comment = new Comment(reviewId, mAuth.getCurrentUser().getUid(), mAuth.getCurrentUser().getDisplayName(), "", rating, comments, date, Integer.parseInt(place_id));
            review.child(place_id).child(reviewId).setValue(comment);
        }

        rate.child(place_id).child(rateId).child("rating").setValue(rating);

        //send average rating
        DatabaseReference ratePlace = database.getReference(PLACE_RATES_ENDPOINT).child(place_id);
        final DatabaseReference place = database.getReference(PLACE_ENDPOINT).child(place_id);

        ratePlace.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                double total = 0.0;
                double count = 0.0;
                double average = 0.0;

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    double rating = Double.parseDouble(ds.child("rating").getValue().toString());
                    total = total + rating;
                    count = count + 1;
                    average = total / count;
                }

                place.child("average_rating").setValue(average);
                onFinishedListener.onFinishedSendReview();
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                onFinishedListener.onErrorSendReview();

            }
        });
    }


}
