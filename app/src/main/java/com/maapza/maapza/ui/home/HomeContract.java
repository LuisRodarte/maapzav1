package com.maapza.maapza.ui.home;

import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.data.remote.model.Place;

import java.util.List;

public interface HomeContract {

    interface View {

        void showCategoryErrorMessage();

        void showPlaceErrorMessage();

        void showLoading(Boolean show);

        void closeActivity();

        void displayListCategories(List<Category> categories);

        void displayListPlaces(List<Place> places);
    }

    interface Presenter {
        void getListCategories();

        void getPlaces();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinishedGetCategories(List<Category> categories);

            void onFinishedGetPlaces(List<Place> places);

            void onErrorGetCategories();

            void onErrorGetPlaces();

            void onFailure(Throwable t);
        }

        void doGetCategories(HomeContract.Interactor.OnFinishedListener onFinishedListener);

        void doGetPlaces(HomeContract.Interactor.OnFinishedListener onFinishedListener);

    }
}
