package com.maapza.maapza.ui.login.signin.verifyphone;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class VerifyPhoneInteractorImpl implements VerifyPhoneContract.Interactor {

    private String mVerificationId = "";
    private Activity activity;

    private FirebaseAuth auth;
    private VerifyPhoneContract.Interactor.OnFinishedListener onFinishedListener;

    public VerifyPhoneInteractorImpl() {
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void verifyPhoneNumber(Activity activity,String phoneNumber, String zipCode, VerifyPhoneContract.Interactor.OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
        this.activity=activity;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                zipCode + phoneNumber,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);

    }

    private void verifyVerificationCode(String code) {
        //creating the credential
        if (!mVerificationId.equals("")) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            //signing the user
            signInWithPhoneAuthCredential(credential);
        } else {
            onFinishedListener.onVerifyError();
        }
    }

    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                onFinishedListener.sendCode(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            onFinishedListener.onVerifyError();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            onFinishedListener.onVerifySuccess();
                        } else {
                            onFinishedListener.onVerifyError();
                        }
                    }
                });
    }


}
