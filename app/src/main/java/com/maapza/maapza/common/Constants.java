package com.maapza.maapza.common;

public class Constants {

    public static final String BUNDLE_PHONE_NUMBER = "PHONE_NUMBER";
    public static final String BUNDLE_ID_CATEGORY = "id_category";
    public static final String BUNDLE_NAME_CATEGORY = "name_category";
    public static final String BUNDLE_ACTION = "action";
    public static final String BUNDLE_ID_PLACE= "id_place";
    public static final String BUNDLE_LATITUDE= "lat";
    public static final String BUNDLE_LONGITUDE= "lng";
    public static final String BUNDLE_ACTION_CATEGORY = "only_category";
    public static final String BUNDLE_ACTION_PLACE = "only_place";
    public static final String DEFAULTPHOTOURL="https://firebasestorage.googleapis.com/v0/b/maapza-d8f1e.appspot.com/o/profileimages%2F9tMoVrQCTVbKDw1YWiMvToq4hNG3.jpg?alt=media&token=1f41fd8d-c0b6-456d-8dad-7b33219ae6ec";

}
